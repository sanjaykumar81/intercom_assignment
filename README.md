# Customer invite program

This program prints list of customers within 100Kms radius of Intercom Dublin office to be invited for the party.


## Requirement 
Java 8 is the programming language and need following to run:

- JDK 8
- JRE
- Java IDE (IntelliJ) 
- gradle 5.2.1

## Test
```bash
cd intercom_assignment
./gradlew clean test
```

## Build
To build the program
```bash
cd intercom_assignment
./gradlew clean build
```

## Run
To run the program execute below command from the project directory after building the application.
```bash
java -jar ./build/bin/customer_invite.jar <absolute path to cutomers.txt file>
```
Note:  
This jar can be copied and executed from anywhere where JRE is present 
A pre-built `customer_invite.jar` is  present in `intercom_assignment/src/test/resources` folder. 

## Output
A sample output of this program is in the output.txt file which is present in `intercom_assignment/src/test/resources` folder.
 
## Additional Info

`Constants` under `utils` contains the default configuration for  distance, lat and lon for this program.


## Proudest Achievement

I believe getting CKA certified was the recent proudest achievement for me. 

Our company wanted to have VMware partnership, and a key requirement to it was 
to have at least two people from the company to be CKA certified. 

Though I come from a development background I volunteered for it as I was interested in learning about Kubernetes.

Certified Kubernetes Administrator is a practical exam and one of the toughest. So, after 2 months of preparation, I was able to crack it out and got CAK certified.

What I like about this achievement is, it not only helped my current company to get VMware partnership but also, helped me in achieving my goal of learning and understanding Kubernters, inforcing my belief that I can learn anything if I want. 