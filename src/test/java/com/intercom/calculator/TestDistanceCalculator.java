package com.intercom.calculator;

import com.intercom.utils.Constants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class TestDistanceCalculator {

    @ParameterizedTest
    @CsvSource({"53.339428,-6.257664,0.0", "52.986375,-6.043701,41768.7845"})
    public void test_distance_calculator(double lat1, double long1, double expected){

        // When
        double actual =DistanceCalculator.calculate(lat1, long1, Constants.DUB_OFFICE_LAT, Constants.DUB_OFFICE_LON);

        // Then
        Assertions.assertEquals(expected,actual,0.00001);
    }
}
