package com.intercom.printer;

import com.intercom.model.Customer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCustomerDataPrinter {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setErr(null);
        System.setOut(null);
    }

    @Test
    public void test_print_customer_data(){
        // Given
        List<Customer> customers = new LinkedList<>();
        customers.add(new Customer( "name1",1,"",""));
        customers.add(new Customer( "name2",2,"",""));
        customers.add(new Customer( "name3",3,"",""));
        customers.add(new Customer( "name4",4,"",""));

        // When
        CustomerDataPrinter printer = new CustomerDataPrinter();
        printer.print(customers);

        // Then
        assertEquals("name1 1\n" +
                "name2 2\n" +
                "name3 3\n" +
                "name4 4\n", outContent.toString());

    }
}
