package com.intercom.reader;

import com.intercom.model.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;

import static com.intercom.utils.Constants.MAX_DISTANCE_FOR_INVITE_METERS;

public class TestCustomerDataReader {

    @ParameterizedTest
    @CsvSource({"53.339428,-6.257664,1", "52.986375,-6.043701,1"})
    public void test_get_customer_in_100kms(String latitude, String longtitude, int expected) {
        //Given
        CsvDataReader csvDataReader = Mockito.mock(CsvDataReader.class);
        Customer customer = new Customer("Test",4,latitude,longtitude);
        List<Customer> customers = new LinkedList<>();
        customers.add(customer);
        Mockito.when(csvDataReader.read()).thenReturn(customers);

        //When
        CustomerDataReader dataReader = new CustomerDataReader(csvDataReader);
        List<Customer> filtered_customers = dataReader.getCustomerWithInRange(MAX_DISTANCE_FOR_INVITE_METERS);

        //Then
        Assertions.assertEquals(expected,filtered_customers.size());
    }

}
