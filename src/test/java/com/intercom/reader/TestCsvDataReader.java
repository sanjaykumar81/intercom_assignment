package com.intercom.reader;

import com.intercom.model.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class TestCsvDataReader {

    @Test
    public void test_read_return_customer_list() throws URISyntaxException {
        // Given
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("customers.txt");
        File file = new File(resource.toURI());

        // When
        CsvDataReader csvReader = new CsvDataReader(file);
        List<Customer> customers = csvReader.read();

        // Then
        Assertions.assertEquals(4, customers.size());

        String [] names  = {"Christina McArdle","Alice Cahill","Ian McArdle","Jack Enright"};
        for (Customer customer: customers) {
            Assertions.assertTrue(Arrays.asList(names).contains(customer.getName()));
        }
    }

    @Test
    public void test_read_return_empty_customer_list_for_empty_file() throws URISyntaxException {
        // Given
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("empty_customers.txt");
        File file = new File(resource.toURI());

        // When
        CsvDataReader csvReader = new CsvDataReader(file);
        List<Customer> customers = csvReader.read();

        // Then
        Assertions.assertEquals(0, customers.size());

    }

    @Test
    public void test_read_return_empty_customer_list_for_invalid_file() throws URISyntaxException {

        // Given
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("invalid_customers.txt");
        File file = new File(resource.toURI());

        // When
        CsvDataReader csvReader = new CsvDataReader(file);
        List<Customer> customers = csvReader.read();

        // Then
        Assertions.assertEquals(0, customers.size());

    }
}
