package com.intercom.utils;

public class Constants {
    public static long EARTH_RADIUS_METERS = 6371009;
    public static final double MAX_DISTANCE_FOR_INVITE_METERS = 100000.0;
    public static final double DUB_OFFICE_LAT = 53.339428;
    public  static final double DUB_OFFICE_LON = -6.257664;
}
