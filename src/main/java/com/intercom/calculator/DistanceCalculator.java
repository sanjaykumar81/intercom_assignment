package com.intercom.calculator;
import com.intercom.utils.Constants;

import static java.lang.Math.*;

public class DistanceCalculator {

    public static double calculate(double lat1, double lon1, double lat2, double lon2){
        double central_angle = acos(
                                    sin(toRadians(lat1))*sin(toRadians(lat2)) +
                                    cos(toRadians(lat1))*cos(toRadians(lat2))*cos(abs(toRadians(lon2)-toRadians(lon1))) );
        double distance =  Constants.EARTH_RADIUS_METERS *central_angle;
        return distance;
    }
}
