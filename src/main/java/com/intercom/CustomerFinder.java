package com.intercom;

import com.intercom.model.Customer;
import com.intercom.printer.CustomerDataPrinter;
import com.intercom.reader.CsvDataReader;
import com.intercom.reader.CustomerDataReader;

import java.io.File;
import java.util.List;

import static com.intercom.utils.Constants.MAX_DISTANCE_FOR_INVITE_METERS;

public class CustomerFinder {

    public static void main(String[] args) {

        if (args.length<1){
            System.out.println("Path to customer.txt file required. Kindly enter the file path as argument.");
        }
        String filePath = args[0];

        CsvDataReader csvDataReader =  new CsvDataReader(new File(filePath));
        CustomerDataReader dataReader = new CustomerDataReader(csvDataReader);
        List<Customer> customers = dataReader.getCustomerWithInRange(MAX_DISTANCE_FOR_INVITE_METERS);
        CustomerDataPrinter printer = new CustomerDataPrinter();
        printer.print(customers);
        }

}