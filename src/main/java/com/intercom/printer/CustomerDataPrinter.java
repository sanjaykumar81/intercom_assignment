package com.intercom.printer;

import com.intercom.model.Customer;

import java.util.Collections;
import java.util.List;

public class CustomerDataPrinter {

    public void print(List<Customer> customers){
        Collections.sort(customers);
        for (Customer customer: customers) {
            System.out.printf("%s %d", customer.getName(),customer.getUser_id());
            System.out.println();

        }

    }


}
