package com.intercom.reader;

import com.intercom.exceptions.InvalidFormatException;
import com.intercom.model.Customer;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


public class CsvDataReader implements DataReader {
    private final File file;

    public CsvDataReader(File file) {
        this.file = file;
    }

    public List<Customer> read() {
        List<Customer> customers = new LinkedList<Customer>();
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String cus_data = scanner.nextLine();
                Gson gson = new Gson();
                JsonElement parser = JsonParser.parseString(cus_data);
                Customer customer = gson.fromJson(parser, Customer.class);
                if (customer.getUser_id() == 0 ||
                        customer.getName() == null ||
                        customer.getLatitude() == null ||
                        customer.getLongitude() == null) {
                    throw new InvalidFormatException("User data is not correct for " + cus_data);
                }
                customers.add(customer);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        return customers;
    }
}
