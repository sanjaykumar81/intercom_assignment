package com.intercom.reader;


import com.intercom.model.Customer;

import java.util.List;

public interface DataReader {
    List<Customer> read();
}
