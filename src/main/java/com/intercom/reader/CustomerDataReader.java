package com.intercom.reader;


import com.intercom.calculator.DistanceCalculator;
import com.intercom.model.Customer;
import com.intercom.utils.Constants;

import java.util.List;
import java.util.stream.Collectors;

public class CustomerDataReader {
    private final DataReader reader;

    public CustomerDataReader(DataReader reader) {
        this.reader = reader;
    }

    public List<Customer> getCustomerWithInRange(double distance_in_meter) {
        List<Customer> customers = reader.read();

        return customers
                .parallelStream()
                .filter(customer ->
                        DistanceCalculator.calculate(
                                new Double(customer.getLatitude()),
                                new Double(customer.getLongitude()),
                                Constants.DUB_OFFICE_LAT,
                                Constants.DUB_OFFICE_LON
                        ) <= distance_in_meter

                ).collect(Collectors.toList());

    }
}
