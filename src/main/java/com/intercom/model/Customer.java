package com.intercom.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor()
public class Customer implements Comparable<Customer> {
    private String name;
    private int user_id;
    private String latitude;
    private String longitude;

    @Override
    public int compareTo(Customer cust) {
        if (this.getUser_id() == cust.getUser_id())
            return 0;
        else if (this.getUser_id() > cust.getUser_id())
            return 1;
        else
            return -1;
    }
}
